json.extract! director, :id, :name, :surname, :birth, :biography, :created_at, :updated_at
json.url director_url(director, format: :json)
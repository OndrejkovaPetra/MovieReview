json.extract! want_to_see, :id, :date, :created_at, :updated_at
json.url want_to_see_url(want_to_see, format: :json)
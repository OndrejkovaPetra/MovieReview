json.extract! actor, :id, :name, :surname, :birth, :biography, :created_at, :updated_at
json.url actor_url(actor, format: :json)
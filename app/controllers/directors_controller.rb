class DirectorsController < ApplicationController
  before_action :set_director, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:show]

  def destroy
    if @director.movies.empty?
      @director.destroy
      respond_to do |format|
        format.html { redirect_to movies_path, notice: 'Director was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to @director, notice: 'Director could not be destroyed (He is assign to some movie).' }
        format.json { render :show, status: :created, location: @director }
      end
    end
  end

  # GET /directors/1
  # GET /directors/1.json
  def show
    @movies = {}
    @director.movies.each do |m|  # Movie.where(director_id: @director.id)
      if @movies.key?(m.year)
        @movies[m.year].add(m)
      else
        @movies[m.year] = Set.new
        @movies[m.year].add(m)
      end
    end
  end

  # GET /directors/new
  def new
    @director = Director.new
  end

  # GET /directors/1/edit
  def edit
  end

  # POST /directors
  # POST /directors.json
  def create
    @director = Director.new(director_params)

    respond_to do |format|
      if @director.save
        format.html { redirect_to @director, notice: 'Director was successfully created.' }
        format.json { render :show, status: :created, location: @director }
      else
        format.html { render :new }
        format.json { render json: @director.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /directors/1
  # PATCH/PUT /directors/1.json
  def update
    respond_to do |format|
      if @director.update(director_params)
        format.html { redirect_to @director, notice: 'Director was successfully updated.' }
        format.json { render :show, status: :ok, location: @director }
      else
        format.html { render :edit }
        format.json { render json: @director.errors, status: :unprocessable_entity }
      end
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_director
      @director = Director.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def director_params
      params.require(:director).permit(:name, :surname, :birth, :biography, :image)
    end
end

class MoviesController < ApplicationController
  before_action :set_movie, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show, :search]

  def index
    if user_signed_in?
      @wantSee = want_this_week
      @wantSee = @wantSee.paginate(page: params[:want_this_week], per_page: 4)
    end
    @movies = Movie.all.order("created_at DESC")
    @movies = @movies[0..4] if @movies.length > 5
  end

  def show
    @write = false
    @want = false
    @reviews = Review.where(movie_id: @movie.id).order("created_at DESC")
    @reviews = @reviews.paginate(page: params[:reviews], per_page: 5)
    if user_signed_in?
      @write = true if Review.where(
        movie_id: @movie.id, user_id: current_user.id).blank?
      @want = true if WantToSee.where(
        movie_id: @movie.id, user_id: current_user.id).blank?
    end
  end

  def new
    @movie = current_user.movies.build
  end

  def edit
  end

  def create
    @movie = current_user.movies.build(movie_params)
    @actors = Actor.where(:id => params[:movie][:actors])
    @movie.actors << @actors

    respond_to do |format|
      if @movie.save
        format.html { redirect_to @movie, notice: 'Movie was successfully created.' }
        format.json { render :show, status: :created, location: @movie }
      else
        format.html { render :new }
        format.json { render json: @movie.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      @movie.user_id = current_user.id
      @actors = Actor.where(:id => params[:movie][:actors])
      @movie.actors.destroy_all
      @movie.actors << @actors

      if @movie.update(movie_params)
        format.html { redirect_to @movie, notice: 'Movie was successfully updated.' }
        format.json { render :show, status: :ok, location: @movie }
      else
        format.html { render :edit }
        format.json { render json: @movie.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @movie.destroy
    respond_to do |format|
      format.html { redirect_to movies_path, notice: 'Movie was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_movie
      @movie = Movie.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def movie_params
      params.require(:movie).permit(:title, :description, :director, :year,
        :lengthMinutes, :image, :category_id, :director_id)
    end

    def want_this_week
      endWeek = Date.today + (7 - Date.today.wday).modulo(7)
      startWeek = endWeek - 6
      WantToSee.where(date: startWeek..endWeek, user_id: current_user.id).order("date ASC")
    end
end

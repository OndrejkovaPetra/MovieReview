class AddMovieIdToWantToSees < ActiveRecord::Migration[5.0]
  def change
    add_column :want_to_sees, :movie_id, :integer
  end
end

class CreateDirectors < ActiveRecord::Migration[5.0]
  def change
    create_table :directors do |t|
      t.string :name
      t.string :surname
      t.date :birth
      t.text :biography

      t.timestamps
    end
  end
end
